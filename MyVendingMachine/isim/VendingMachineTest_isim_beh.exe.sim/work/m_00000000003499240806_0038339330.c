/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Users/saina/Desktop/SainaDaneshmandJahromiFinalProject/MyVendingMachine/VendingMachine.v";
static const char *ng1 = "MyVending.txt";
static int ng2[] = {0, 0};
static unsigned int ng3[] = {1U, 0U};
static int ng4[] = {5, 0};
static int ng5[] = {3, 0};
static const char *ng6 = "The item is bought successfully";
static const char *ng7 = "You don't have enough money you need to have %b money";
static unsigned int ng8[] = {0U, 0U};
static const char *ng9 = "We just have %b of this item";
static int ng10[] = {1, 0};
static int ng11[] = {2, 0};
static const char *ng12 = "We don't have this item in our vending machine";
static const char *ng13 = "w";
static const char *ng14 = "%b\n";



static void Initial_25_0(char *t0)
{
    char *t1;

LAB0:    xsi_set_current_line(26, ng0);

LAB2:    xsi_set_current_line(27, ng0);
    t1 = (t0 + 1768);
    xsi_vlogfile_readmemb(ng1, 0, t1, 0, 0, 0, 0);

LAB1:    return;
}

static void Always_39_1(char *t0)
{
    char t7[8];
    char t15[8];
    char t26[8];
    char t50[8];
    char t58[8];
    char t69[8];
    char t83[8];
    char t91[8];
    char t100[8];
    char t101[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t16;
    char *t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    unsigned int t22;
    unsigned int t23;
    char *t24;
    char *t25;
    char *t27;
    unsigned int t28;
    unsigned int t29;
    unsigned int t30;
    unsigned int t31;
    unsigned int t32;
    unsigned int t33;
    unsigned int t34;
    unsigned int t35;
    unsigned int t36;
    unsigned int t37;
    unsigned int t38;
    unsigned int t39;
    char *t40;
    char *t41;
    unsigned int t42;
    unsigned int t43;
    unsigned int t44;
    unsigned int t45;
    unsigned int t46;
    char *t47;
    char *t48;
    char *t49;
    char *t51;
    char *t52;
    char *t53;
    char *t54;
    char *t55;
    char *t56;
    char *t57;
    char *t59;
    char *t60;
    unsigned int t61;
    unsigned int t62;
    unsigned int t63;
    unsigned int t64;
    unsigned int t65;
    unsigned int t66;
    char *t67;
    char *t68;
    char *t70;
    char *t71;
    char *t72;
    unsigned int t73;
    unsigned int t74;
    unsigned int t75;
    unsigned int t76;
    unsigned int t77;
    char *t78;
    char *t79;
    char *t80;
    char *t81;
    char *t82;
    char *t84;
    char *t85;
    char *t86;
    char *t87;
    char *t88;
    char *t89;
    char *t90;
    char *t92;
    char *t93;
    unsigned int t94;
    unsigned int t95;
    unsigned int t96;
    unsigned int t97;
    unsigned int t98;
    unsigned int t99;
    char *t102;
    char *t103;
    char *t104;
    char *t105;
    unsigned int t106;
    unsigned int t107;
    unsigned int t108;
    unsigned int t109;
    unsigned int t110;
    char *t111;
    char *t112;
    int t113;
    int t114;
    int t115;
    int t116;
    int t117;
    int t118;
    int t119;
    int t120;
    int t121;
    int t122;
    int t123;
    int t124;
    int t125;

LAB0:    t1 = (t0 + 3416U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(39, ng0);
    t2 = (t0 + 4480);
    *((int *)t2) = 1;
    t3 = (t0 + 3448);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(40, ng0);

LAB5:    xsi_set_current_line(41, ng0);
    t4 = (t0 + 1768);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t8 = (t0 + 1768);
    t9 = (t8 + 72U);
    t10 = *((char **)t9);
    t11 = (t0 + 1768);
    t12 = (t11 + 64U);
    t13 = *((char **)t12);
    t14 = ((char*)((ng2)));
    xsi_vlog_generic_get_array_select_value(t7, 8, t6, t10, t13, 2, 1, t14, 32, 1);
    memset(t15, 0, 8);
    t16 = (t15 + 4);
    t17 = (t7 + 4);
    t18 = *((unsigned int *)t7);
    t19 = (t18 >> 6);
    *((unsigned int *)t15) = t19;
    t20 = *((unsigned int *)t17);
    t21 = (t20 >> 6);
    *((unsigned int *)t16) = t21;
    t22 = *((unsigned int *)t15);
    *((unsigned int *)t15) = (t22 & 3U);
    t23 = *((unsigned int *)t16);
    *((unsigned int *)t16) = (t23 & 3U);
    t24 = (t0 + 1048U);
    t25 = *((char **)t24);
    memset(t26, 0, 8);
    t24 = (t15 + 4);
    t27 = (t25 + 4);
    t28 = *((unsigned int *)t15);
    t29 = *((unsigned int *)t25);
    t30 = (t28 ^ t29);
    t31 = *((unsigned int *)t24);
    t32 = *((unsigned int *)t27);
    t33 = (t31 ^ t32);
    t34 = (t30 | t33);
    t35 = *((unsigned int *)t24);
    t36 = *((unsigned int *)t27);
    t37 = (t35 | t36);
    t38 = (~(t37));
    t39 = (t34 & t38);
    if (t39 != 0)
        goto LAB9;

LAB6:    if (t37 != 0)
        goto LAB8;

LAB7:    *((unsigned int *)t26) = 1;

LAB9:    t41 = (t26 + 4);
    t42 = *((unsigned int *)t41);
    t43 = (~(t42));
    t44 = *((unsigned int *)t26);
    t45 = (t44 & t43);
    t46 = (t45 != 0);
    if (t46 > 0)
        goto LAB10;

LAB11:    xsi_set_current_line(66, ng0);
    t2 = (t0 + 1768);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t0 + 1768);
    t6 = (t5 + 72U);
    t8 = *((char **)t6);
    t9 = (t0 + 1768);
    t10 = (t9 + 64U);
    t11 = *((char **)t10);
    t12 = ((char*)((ng10)));
    xsi_vlog_generic_get_array_select_value(t7, 8, t4, t8, t11, 2, 1, t12, 32, 1);
    memset(t15, 0, 8);
    t13 = (t15 + 4);
    t14 = (t7 + 4);
    t18 = *((unsigned int *)t7);
    t19 = (t18 >> 6);
    *((unsigned int *)t15) = t19;
    t20 = *((unsigned int *)t14);
    t21 = (t20 >> 6);
    *((unsigned int *)t13) = t21;
    t22 = *((unsigned int *)t15);
    *((unsigned int *)t15) = (t22 & 3U);
    t23 = *((unsigned int *)t13);
    *((unsigned int *)t13) = (t23 & 3U);
    t16 = (t0 + 1048U);
    t17 = *((char **)t16);
    memset(t26, 0, 8);
    t16 = (t15 + 4);
    t24 = (t17 + 4);
    t28 = *((unsigned int *)t15);
    t29 = *((unsigned int *)t17);
    t30 = (t28 ^ t29);
    t31 = *((unsigned int *)t16);
    t32 = *((unsigned int *)t24);
    t33 = (t31 ^ t32);
    t34 = (t30 | t33);
    t35 = *((unsigned int *)t16);
    t36 = *((unsigned int *)t24);
    t37 = (t35 | t36);
    t38 = (~(t37));
    t39 = (t34 & t38);
    if (t39 != 0)
        goto LAB39;

LAB36:    if (t37 != 0)
        goto LAB38;

LAB37:    *((unsigned int *)t26) = 1;

LAB39:    t27 = (t26 + 4);
    t42 = *((unsigned int *)t27);
    t43 = (~(t42));
    t44 = *((unsigned int *)t26);
    t45 = (t44 & t43);
    t46 = (t45 != 0);
    if (t46 > 0)
        goto LAB40;

LAB41:    xsi_set_current_line(91, ng0);
    t2 = (t0 + 1768);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t0 + 1768);
    t6 = (t5 + 72U);
    t8 = *((char **)t6);
    t9 = (t0 + 1768);
    t10 = (t9 + 64U);
    t11 = *((char **)t10);
    t12 = ((char*)((ng11)));
    xsi_vlog_generic_get_array_select_value(t7, 8, t4, t8, t11, 2, 1, t12, 32, 1);
    memset(t15, 0, 8);
    t13 = (t15 + 4);
    t14 = (t7 + 4);
    t18 = *((unsigned int *)t7);
    t19 = (t18 >> 6);
    *((unsigned int *)t15) = t19;
    t20 = *((unsigned int *)t14);
    t21 = (t20 >> 6);
    *((unsigned int *)t13) = t21;
    t22 = *((unsigned int *)t15);
    *((unsigned int *)t15) = (t22 & 3U);
    t23 = *((unsigned int *)t13);
    *((unsigned int *)t13) = (t23 & 3U);
    t16 = (t0 + 1048U);
    t17 = *((char **)t16);
    memset(t26, 0, 8);
    t16 = (t15 + 4);
    t24 = (t17 + 4);
    t28 = *((unsigned int *)t15);
    t29 = *((unsigned int *)t17);
    t30 = (t28 ^ t29);
    t31 = *((unsigned int *)t16);
    t32 = *((unsigned int *)t24);
    t33 = (t31 ^ t32);
    t34 = (t30 | t33);
    t35 = *((unsigned int *)t16);
    t36 = *((unsigned int *)t24);
    t37 = (t35 | t36);
    t38 = (~(t37));
    t39 = (t34 & t38);
    if (t39 != 0)
        goto LAB69;

LAB66:    if (t37 != 0)
        goto LAB68;

LAB67:    *((unsigned int *)t26) = 1;

LAB69:    t27 = (t26 + 4);
    t42 = *((unsigned int *)t27);
    t43 = (~(t42));
    t44 = *((unsigned int *)t26);
    t45 = (t44 & t43);
    t46 = (t45 != 0);
    if (t46 > 0)
        goto LAB70;

LAB71:    xsi_set_current_line(116, ng0);
    t2 = (t0 + 1768);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t0 + 1768);
    t6 = (t5 + 72U);
    t8 = *((char **)t6);
    t9 = (t0 + 1768);
    t10 = (t9 + 64U);
    t11 = *((char **)t10);
    t12 = ((char*)((ng5)));
    xsi_vlog_generic_get_array_select_value(t7, 8, t4, t8, t11, 2, 1, t12, 32, 1);
    memset(t15, 0, 8);
    t13 = (t15 + 4);
    t14 = (t7 + 4);
    t18 = *((unsigned int *)t7);
    t19 = (t18 >> 6);
    *((unsigned int *)t15) = t19;
    t20 = *((unsigned int *)t14);
    t21 = (t20 >> 6);
    *((unsigned int *)t13) = t21;
    t22 = *((unsigned int *)t15);
    *((unsigned int *)t15) = (t22 & 3U);
    t23 = *((unsigned int *)t13);
    *((unsigned int *)t13) = (t23 & 3U);
    t16 = (t0 + 1048U);
    t17 = *((char **)t16);
    memset(t26, 0, 8);
    t16 = (t15 + 4);
    t24 = (t17 + 4);
    t28 = *((unsigned int *)t15);
    t29 = *((unsigned int *)t17);
    t30 = (t28 ^ t29);
    t31 = *((unsigned int *)t16);
    t32 = *((unsigned int *)t24);
    t33 = (t31 ^ t32);
    t34 = (t30 | t33);
    t35 = *((unsigned int *)t16);
    t36 = *((unsigned int *)t24);
    t37 = (t35 | t36);
    t38 = (~(t37));
    t39 = (t34 & t38);
    if (t39 != 0)
        goto LAB99;

LAB96:    if (t37 != 0)
        goto LAB98;

LAB97:    *((unsigned int *)t26) = 1;

LAB99:    t27 = (t26 + 4);
    t42 = *((unsigned int *)t27);
    t43 = (~(t42));
    t44 = *((unsigned int *)t26);
    t45 = (t44 & t43);
    t46 = (t45 != 0);
    if (t46 > 0)
        goto LAB100;

LAB101:    xsi_set_current_line(142, ng0);

LAB126:    xsi_set_current_line(143, ng0);
    t2 = ((char*)((ng8)));
    t3 = (t0 + 1928);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    xsi_set_current_line(144, ng0);
    t2 = (t0 + 1368U);
    t3 = *((char **)t2);
    t2 = (t0 + 2088);
    xsi_vlogvar_assign_value(t2, t3, 0, 0, 4);
    xsi_set_current_line(145, ng0);
    xsi_vlogfile_write(1, 0, 0, ng12, 1, t0);

LAB102:
LAB72:
LAB42:
LAB12:    goto LAB2;

LAB8:    t40 = (t26 + 4);
    *((unsigned int *)t26) = 1;
    *((unsigned int *)t40) = 1;
    goto LAB9;

LAB10:    xsi_set_current_line(42, ng0);

LAB13:    xsi_set_current_line(43, ng0);
    t47 = (t0 + 1768);
    t48 = (t47 + 56U);
    t49 = *((char **)t48);
    t51 = (t0 + 1768);
    t52 = (t51 + 72U);
    t53 = *((char **)t52);
    t54 = (t0 + 1768);
    t55 = (t54 + 64U);
    t56 = *((char **)t55);
    t57 = ((char*)((ng2)));
    xsi_vlog_generic_get_array_select_value(t50, 8, t49, t53, t56, 2, 1, t57, 32, 1);
    memset(t58, 0, 8);
    t59 = (t58 + 4);
    t60 = (t50 + 4);
    t61 = *((unsigned int *)t50);
    t62 = (t61 >> 3);
    *((unsigned int *)t58) = t62;
    t63 = *((unsigned int *)t60);
    t64 = (t63 >> 3);
    *((unsigned int *)t59) = t64;
    t65 = *((unsigned int *)t58);
    *((unsigned int *)t58) = (t65 & 7U);
    t66 = *((unsigned int *)t59);
    *((unsigned int *)t59) = (t66 & 7U);
    t67 = (t0 + 1208U);
    t68 = *((char **)t67);
    memset(t69, 0, 8);
    t67 = (t58 + 4);
    if (*((unsigned int *)t67) != 0)
        goto LAB15;

LAB14:    t70 = (t68 + 4);
    if (*((unsigned int *)t70) != 0)
        goto LAB15;

LAB18:    if (*((unsigned int *)t58) < *((unsigned int *)t68))
        goto LAB17;

LAB16:    *((unsigned int *)t69) = 1;

LAB17:    t72 = (t69 + 4);
    t73 = *((unsigned int *)t72);
    t74 = (~(t73));
    t75 = *((unsigned int *)t69);
    t76 = (t75 & t74);
    t77 = (t76 != 0);
    if (t77 > 0)
        goto LAB19;

LAB20:    xsi_set_current_line(60, ng0);

LAB35:    xsi_set_current_line(61, ng0);
    t2 = ((char*)((ng8)));
    t3 = (t0 + 1928);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    xsi_set_current_line(62, ng0);
    t2 = (t0 + 1368U);
    t3 = *((char **)t2);
    t2 = (t0 + 2088);
    xsi_vlogvar_assign_value(t2, t3, 0, 0, 4);
    xsi_set_current_line(63, ng0);
    t2 = (t0 + 1208U);
    t3 = *((char **)t2);
    t2 = (t0 + 1768);
    t4 = (t2 + 56U);
    t5 = *((char **)t4);
    t6 = (t0 + 1768);
    t8 = (t6 + 72U);
    t9 = *((char **)t8);
    t10 = (t0 + 1768);
    t11 = (t10 + 64U);
    t12 = *((char **)t11);
    t13 = ((char*)((ng2)));
    xsi_vlog_generic_get_array_select_value(t7, 8, t5, t9, t12, 2, 1, t13, 32, 1);
    memset(t15, 0, 8);
    t14 = (t15 + 4);
    t16 = (t7 + 4);
    t18 = *((unsigned int *)t7);
    t19 = (t18 >> 3);
    *((unsigned int *)t15) = t19;
    t20 = *((unsigned int *)t16);
    t21 = (t20 >> 3);
    *((unsigned int *)t14) = t21;
    t22 = *((unsigned int *)t15);
    *((unsigned int *)t15) = (t22 & 7U);
    t23 = *((unsigned int *)t14);
    *((unsigned int *)t14) = (t23 & 7U);
    memset(t26, 0, 8);
    xsi_vlog_unsigned_multiply(t26, 3, t3, 3, t15, 3);
    xsi_vlogfile_write(1, 0, 0, ng9, 2, t0, (char)118, t26, 3);

LAB21:    goto LAB12;

LAB15:    t71 = (t69 + 4);
    *((unsigned int *)t69) = 1;
    *((unsigned int *)t71) = 1;
    goto LAB17;

LAB19:    xsi_set_current_line(44, ng0);

LAB22:    xsi_set_current_line(45, ng0);
    t78 = (t0 + 1368U);
    t79 = *((char **)t78);
    t78 = (t0 + 1208U);
    t80 = *((char **)t78);
    t78 = (t0 + 1768);
    t81 = (t78 + 56U);
    t82 = *((char **)t81);
    t84 = (t0 + 1768);
    t85 = (t84 + 72U);
    t86 = *((char **)t85);
    t87 = (t0 + 1768);
    t88 = (t87 + 64U);
    t89 = *((char **)t88);
    t90 = ((char*)((ng2)));
    xsi_vlog_generic_get_array_select_value(t83, 8, t82, t86, t89, 2, 1, t90, 32, 1);
    memset(t91, 0, 8);
    t92 = (t91 + 4);
    t93 = (t83 + 4);
    t94 = *((unsigned int *)t83);
    t95 = (t94 >> 0);
    *((unsigned int *)t91) = t95;
    t96 = *((unsigned int *)t93);
    t97 = (t96 >> 0);
    *((unsigned int *)t92) = t97;
    t98 = *((unsigned int *)t91);
    *((unsigned int *)t91) = (t98 & 7U);
    t99 = *((unsigned int *)t92);
    *((unsigned int *)t92) = (t99 & 7U);
    memset(t100, 0, 8);
    xsi_vlog_unsigned_multiply(t100, 4, t80, 3, t91, 4);
    memset(t101, 0, 8);
    t102 = (t79 + 4);
    if (*((unsigned int *)t102) != 0)
        goto LAB24;

LAB23:    t103 = (t100 + 4);
    if (*((unsigned int *)t103) != 0)
        goto LAB24;

LAB27:    if (*((unsigned int *)t79) < *((unsigned int *)t100))
        goto LAB26;

LAB25:    *((unsigned int *)t101) = 1;

LAB26:    t105 = (t101 + 4);
    t106 = *((unsigned int *)t105);
    t107 = (~(t106));
    t108 = *((unsigned int *)t101);
    t109 = (t108 & t107);
    t110 = (t109 != 0);
    if (t110 > 0)
        goto LAB28;

LAB29:    xsi_set_current_line(53, ng0);

LAB34:    xsi_set_current_line(54, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 1928);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    xsi_set_current_line(55, ng0);
    t2 = (t0 + 1368U);
    t3 = *((char **)t2);
    t2 = (t0 + 2088);
    xsi_vlogvar_assign_value(t2, t3, 0, 0, 4);
    xsi_set_current_line(56, ng0);
    t2 = (t0 + 1208U);
    t3 = *((char **)t2);
    t2 = (t0 + 1768);
    t4 = (t2 + 56U);
    t5 = *((char **)t4);
    t6 = (t0 + 1768);
    t8 = (t6 + 72U);
    t9 = *((char **)t8);
    t10 = (t0 + 1768);
    t11 = (t10 + 64U);
    t12 = *((char **)t11);
    t13 = ((char*)((ng2)));
    xsi_vlog_generic_get_array_select_value(t7, 8, t5, t9, t12, 2, 1, t13, 32, 1);
    memset(t15, 0, 8);
    t14 = (t15 + 4);
    t16 = (t7 + 4);
    t18 = *((unsigned int *)t7);
    t19 = (t18 >> 0);
    *((unsigned int *)t15) = t19;
    t20 = *((unsigned int *)t16);
    t21 = (t20 >> 0);
    *((unsigned int *)t14) = t21;
    t22 = *((unsigned int *)t15);
    *((unsigned int *)t15) = (t22 & 7U);
    t23 = *((unsigned int *)t14);
    *((unsigned int *)t14) = (t23 & 7U);
    memset(t26, 0, 8);
    xsi_vlog_unsigned_multiply(t26, 3, t3, 3, t15, 3);
    xsi_vlogfile_write(1, 0, 0, ng7, 2, t0, (char)118, t26, 3);

LAB30:    goto LAB21;

LAB24:    t104 = (t101 + 4);
    *((unsigned int *)t101) = 1;
    *((unsigned int *)t104) = 1;
    goto LAB26;

LAB28:    xsi_set_current_line(46, ng0);

LAB31:    xsi_set_current_line(47, ng0);
    t111 = ((char*)((ng3)));
    t112 = (t0 + 1928);
    xsi_vlogvar_assign_value(t112, t111, 0, 0, 1);
    xsi_set_current_line(48, ng0);
    t2 = (t0 + 1368U);
    t3 = *((char **)t2);
    t2 = (t0 + 1208U);
    t4 = *((char **)t2);
    t2 = (t0 + 1768);
    t5 = (t2 + 56U);
    t6 = *((char **)t5);
    t8 = (t0 + 1768);
    t9 = (t8 + 72U);
    t10 = *((char **)t9);
    t11 = (t0 + 1768);
    t12 = (t11 + 64U);
    t13 = *((char **)t12);
    t14 = ((char*)((ng2)));
    xsi_vlog_generic_get_array_select_value(t7, 8, t6, t10, t13, 2, 1, t14, 32, 1);
    memset(t15, 0, 8);
    t16 = (t15 + 4);
    t17 = (t7 + 4);
    t18 = *((unsigned int *)t7);
    t19 = (t18 >> 0);
    *((unsigned int *)t15) = t19;
    t20 = *((unsigned int *)t17);
    t21 = (t20 >> 0);
    *((unsigned int *)t16) = t21;
    t22 = *((unsigned int *)t15);
    *((unsigned int *)t15) = (t22 & 7U);
    t23 = *((unsigned int *)t16);
    *((unsigned int *)t16) = (t23 & 7U);
    memset(t26, 0, 8);
    xsi_vlog_unsigned_multiply(t26, 4, t4, 3, t15, 4);
    memset(t50, 0, 8);
    xsi_vlog_unsigned_minus(t50, 4, t3, 4, t26, 4);
    t24 = (t0 + 2088);
    xsi_vlogvar_assign_value(t24, t50, 0, 0, 4);
    xsi_set_current_line(49, ng0);
    t2 = (t0 + 1768);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t0 + 1768);
    t6 = (t5 + 72U);
    t8 = *((char **)t6);
    t9 = (t0 + 1768);
    t10 = (t9 + 64U);
    t11 = *((char **)t10);
    t12 = ((char*)((ng2)));
    xsi_vlog_generic_get_array_select_value(t7, 8, t4, t8, t11, 2, 1, t12, 32, 1);
    memset(t15, 0, 8);
    t13 = (t15 + 4);
    t14 = (t7 + 4);
    t18 = *((unsigned int *)t7);
    t19 = (t18 >> 3);
    *((unsigned int *)t15) = t19;
    t20 = *((unsigned int *)t14);
    t21 = (t20 >> 3);
    *((unsigned int *)t13) = t21;
    t22 = *((unsigned int *)t15);
    *((unsigned int *)t15) = (t22 & 7U);
    t23 = *((unsigned int *)t13);
    *((unsigned int *)t13) = (t23 & 7U);
    t16 = (t0 + 1208U);
    t17 = *((char **)t16);
    memset(t26, 0, 8);
    xsi_vlog_unsigned_minus(t26, 3, t15, 3, t17, 3);
    t16 = (t0 + 1768);
    t24 = (t0 + 1768);
    t25 = (t24 + 72U);
    t27 = *((char **)t25);
    t40 = (t0 + 1768);
    t41 = (t40 + 64U);
    t47 = *((char **)t41);
    t48 = ((char*)((ng2)));
    xsi_vlog_generic_convert_array_indices(t50, t58, t27, t47, 2, 1, t48, 32, 1);
    t49 = (t0 + 1768);
    t51 = (t49 + 72U);
    t52 = *((char **)t51);
    t53 = ((char*)((ng4)));
    t54 = ((char*)((ng5)));
    xsi_vlog_convert_partindices(t69, t83, t91, ((int*)(t52)), 2, t53, 32, 1, t54, 32, 1);
    t55 = (t50 + 4);
    t28 = *((unsigned int *)t55);
    t113 = (!(t28));
    t56 = (t58 + 4);
    t29 = *((unsigned int *)t56);
    t114 = (!(t29));
    t115 = (t113 && t114);
    t57 = (t69 + 4);
    t30 = *((unsigned int *)t57);
    t116 = (!(t30));
    t117 = (t115 && t116);
    t59 = (t83 + 4);
    t31 = *((unsigned int *)t59);
    t118 = (!(t31));
    t119 = (t117 && t118);
    t60 = (t91 + 4);
    t32 = *((unsigned int *)t60);
    t120 = (!(t32));
    t121 = (t119 && t120);
    if (t121 == 1)
        goto LAB32;

LAB33:    xsi_set_current_line(50, ng0);
    xsi_vlogfile_write(1, 0, 0, ng6, 1, t0);
    goto LAB30;

LAB32:    t33 = *((unsigned int *)t91);
    t122 = (t33 + 0);
    t34 = *((unsigned int *)t58);
    t35 = *((unsigned int *)t83);
    t123 = (t34 + t35);
    t36 = *((unsigned int *)t69);
    t37 = *((unsigned int *)t83);
    t124 = (t36 - t37);
    t125 = (t124 + 1);
    xsi_vlogvar_assign_value(t16, t26, t122, t123, t125);
    goto LAB33;

LAB38:    t25 = (t26 + 4);
    *((unsigned int *)t26) = 1;
    *((unsigned int *)t25) = 1;
    goto LAB39;

LAB40:    xsi_set_current_line(67, ng0);

LAB43:    xsi_set_current_line(68, ng0);
    t40 = (t0 + 1768);
    t41 = (t40 + 56U);
    t47 = *((char **)t41);
    t48 = (t0 + 1768);
    t49 = (t48 + 72U);
    t51 = *((char **)t49);
    t52 = (t0 + 1768);
    t53 = (t52 + 64U);
    t54 = *((char **)t53);
    t55 = ((char*)((ng10)));
    xsi_vlog_generic_get_array_select_value(t50, 8, t47, t51, t54, 2, 1, t55, 32, 1);
    memset(t58, 0, 8);
    t56 = (t58 + 4);
    t57 = (t50 + 4);
    t61 = *((unsigned int *)t50);
    t62 = (t61 >> 3);
    *((unsigned int *)t58) = t62;
    t63 = *((unsigned int *)t57);
    t64 = (t63 >> 3);
    *((unsigned int *)t56) = t64;
    t65 = *((unsigned int *)t58);
    *((unsigned int *)t58) = (t65 & 7U);
    t66 = *((unsigned int *)t56);
    *((unsigned int *)t56) = (t66 & 7U);
    t59 = (t0 + 1208U);
    t60 = *((char **)t59);
    memset(t69, 0, 8);
    t59 = (t58 + 4);
    if (*((unsigned int *)t59) != 0)
        goto LAB45;

LAB44:    t67 = (t60 + 4);
    if (*((unsigned int *)t67) != 0)
        goto LAB45;

LAB48:    if (*((unsigned int *)t58) < *((unsigned int *)t60))
        goto LAB47;

LAB46:    *((unsigned int *)t69) = 1;

LAB47:    t70 = (t69 + 4);
    t73 = *((unsigned int *)t70);
    t74 = (~(t73));
    t75 = *((unsigned int *)t69);
    t76 = (t75 & t74);
    t77 = (t76 != 0);
    if (t77 > 0)
        goto LAB49;

LAB50:    xsi_set_current_line(85, ng0);

LAB65:    xsi_set_current_line(86, ng0);
    t2 = ((char*)((ng8)));
    t3 = (t0 + 1928);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    xsi_set_current_line(87, ng0);
    t2 = (t0 + 1368U);
    t3 = *((char **)t2);
    t2 = (t0 + 2088);
    xsi_vlogvar_assign_value(t2, t3, 0, 0, 4);
    xsi_set_current_line(88, ng0);
    t2 = (t0 + 1208U);
    t3 = *((char **)t2);
    t2 = (t0 + 1768);
    t4 = (t2 + 56U);
    t5 = *((char **)t4);
    t6 = (t0 + 1768);
    t8 = (t6 + 72U);
    t9 = *((char **)t8);
    t10 = (t0 + 1768);
    t11 = (t10 + 64U);
    t12 = *((char **)t11);
    t13 = ((char*)((ng10)));
    xsi_vlog_generic_get_array_select_value(t7, 8, t5, t9, t12, 2, 1, t13, 32, 1);
    memset(t15, 0, 8);
    t14 = (t15 + 4);
    t16 = (t7 + 4);
    t18 = *((unsigned int *)t7);
    t19 = (t18 >> 3);
    *((unsigned int *)t15) = t19;
    t20 = *((unsigned int *)t16);
    t21 = (t20 >> 3);
    *((unsigned int *)t14) = t21;
    t22 = *((unsigned int *)t15);
    *((unsigned int *)t15) = (t22 & 7U);
    t23 = *((unsigned int *)t14);
    *((unsigned int *)t14) = (t23 & 7U);
    memset(t26, 0, 8);
    xsi_vlog_unsigned_multiply(t26, 3, t3, 3, t15, 3);
    xsi_vlogfile_write(1, 0, 0, ng9, 2, t0, (char)118, t26, 3);

LAB51:    goto LAB42;

LAB45:    t68 = (t69 + 4);
    *((unsigned int *)t69) = 1;
    *((unsigned int *)t68) = 1;
    goto LAB47;

LAB49:    xsi_set_current_line(69, ng0);

LAB52:    xsi_set_current_line(70, ng0);
    t71 = (t0 + 1368U);
    t72 = *((char **)t71);
    t71 = (t0 + 1208U);
    t78 = *((char **)t71);
    t71 = (t0 + 1768);
    t79 = (t71 + 56U);
    t80 = *((char **)t79);
    t81 = (t0 + 1768);
    t82 = (t81 + 72U);
    t84 = *((char **)t82);
    t85 = (t0 + 1768);
    t86 = (t85 + 64U);
    t87 = *((char **)t86);
    t88 = ((char*)((ng10)));
    xsi_vlog_generic_get_array_select_value(t83, 8, t80, t84, t87, 2, 1, t88, 32, 1);
    memset(t91, 0, 8);
    t89 = (t91 + 4);
    t90 = (t83 + 4);
    t94 = *((unsigned int *)t83);
    t95 = (t94 >> 0);
    *((unsigned int *)t91) = t95;
    t96 = *((unsigned int *)t90);
    t97 = (t96 >> 0);
    *((unsigned int *)t89) = t97;
    t98 = *((unsigned int *)t91);
    *((unsigned int *)t91) = (t98 & 7U);
    t99 = *((unsigned int *)t89);
    *((unsigned int *)t89) = (t99 & 7U);
    memset(t100, 0, 8);
    xsi_vlog_unsigned_multiply(t100, 4, t78, 3, t91, 4);
    memset(t101, 0, 8);
    t92 = (t72 + 4);
    if (*((unsigned int *)t92) != 0)
        goto LAB54;

LAB53:    t93 = (t100 + 4);
    if (*((unsigned int *)t93) != 0)
        goto LAB54;

LAB57:    if (*((unsigned int *)t72) < *((unsigned int *)t100))
        goto LAB56;

LAB55:    *((unsigned int *)t101) = 1;

LAB56:    t103 = (t101 + 4);
    t106 = *((unsigned int *)t103);
    t107 = (~(t106));
    t108 = *((unsigned int *)t101);
    t109 = (t108 & t107);
    t110 = (t109 != 0);
    if (t110 > 0)
        goto LAB58;

LAB59:    xsi_set_current_line(78, ng0);

LAB64:    xsi_set_current_line(79, ng0);
    t2 = ((char*)((ng8)));
    t3 = (t0 + 1928);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    xsi_set_current_line(80, ng0);
    t2 = (t0 + 1368U);
    t3 = *((char **)t2);
    t2 = (t0 + 2088);
    xsi_vlogvar_assign_value(t2, t3, 0, 0, 4);
    xsi_set_current_line(81, ng0);
    t2 = (t0 + 1208U);
    t3 = *((char **)t2);
    t2 = (t0 + 1768);
    t4 = (t2 + 56U);
    t5 = *((char **)t4);
    t6 = (t0 + 1768);
    t8 = (t6 + 72U);
    t9 = *((char **)t8);
    t10 = (t0 + 1768);
    t11 = (t10 + 64U);
    t12 = *((char **)t11);
    t13 = ((char*)((ng10)));
    xsi_vlog_generic_get_array_select_value(t7, 8, t5, t9, t12, 2, 1, t13, 32, 1);
    memset(t15, 0, 8);
    t14 = (t15 + 4);
    t16 = (t7 + 4);
    t18 = *((unsigned int *)t7);
    t19 = (t18 >> 0);
    *((unsigned int *)t15) = t19;
    t20 = *((unsigned int *)t16);
    t21 = (t20 >> 0);
    *((unsigned int *)t14) = t21;
    t22 = *((unsigned int *)t15);
    *((unsigned int *)t15) = (t22 & 7U);
    t23 = *((unsigned int *)t14);
    *((unsigned int *)t14) = (t23 & 7U);
    memset(t26, 0, 8);
    xsi_vlog_unsigned_multiply(t26, 3, t3, 3, t15, 3);
    xsi_vlogfile_write(1, 0, 0, ng7, 2, t0, (char)118, t26, 3);

LAB60:    goto LAB51;

LAB54:    t102 = (t101 + 4);
    *((unsigned int *)t101) = 1;
    *((unsigned int *)t102) = 1;
    goto LAB56;

LAB58:    xsi_set_current_line(71, ng0);

LAB61:    xsi_set_current_line(72, ng0);
    t104 = ((char*)((ng3)));
    t105 = (t0 + 1928);
    xsi_vlogvar_assign_value(t105, t104, 0, 0, 1);
    xsi_set_current_line(73, ng0);
    t2 = (t0 + 1368U);
    t3 = *((char **)t2);
    t2 = (t0 + 1208U);
    t4 = *((char **)t2);
    t2 = (t0 + 1768);
    t5 = (t2 + 56U);
    t6 = *((char **)t5);
    t8 = (t0 + 1768);
    t9 = (t8 + 72U);
    t10 = *((char **)t9);
    t11 = (t0 + 1768);
    t12 = (t11 + 64U);
    t13 = *((char **)t12);
    t14 = ((char*)((ng10)));
    xsi_vlog_generic_get_array_select_value(t7, 8, t6, t10, t13, 2, 1, t14, 32, 1);
    memset(t15, 0, 8);
    t16 = (t15 + 4);
    t17 = (t7 + 4);
    t18 = *((unsigned int *)t7);
    t19 = (t18 >> 0);
    *((unsigned int *)t15) = t19;
    t20 = *((unsigned int *)t17);
    t21 = (t20 >> 0);
    *((unsigned int *)t16) = t21;
    t22 = *((unsigned int *)t15);
    *((unsigned int *)t15) = (t22 & 7U);
    t23 = *((unsigned int *)t16);
    *((unsigned int *)t16) = (t23 & 7U);
    memset(t26, 0, 8);
    xsi_vlog_unsigned_multiply(t26, 4, t4, 3, t15, 4);
    memset(t50, 0, 8);
    xsi_vlog_unsigned_minus(t50, 4, t3, 4, t26, 4);
    t24 = (t0 + 2088);
    xsi_vlogvar_assign_value(t24, t50, 0, 0, 4);
    xsi_set_current_line(74, ng0);
    t2 = (t0 + 1768);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t0 + 1768);
    t6 = (t5 + 72U);
    t8 = *((char **)t6);
    t9 = (t0 + 1768);
    t10 = (t9 + 64U);
    t11 = *((char **)t10);
    t12 = ((char*)((ng10)));
    xsi_vlog_generic_get_array_select_value(t7, 8, t4, t8, t11, 2, 1, t12, 32, 1);
    memset(t15, 0, 8);
    t13 = (t15 + 4);
    t14 = (t7 + 4);
    t18 = *((unsigned int *)t7);
    t19 = (t18 >> 3);
    *((unsigned int *)t15) = t19;
    t20 = *((unsigned int *)t14);
    t21 = (t20 >> 3);
    *((unsigned int *)t13) = t21;
    t22 = *((unsigned int *)t15);
    *((unsigned int *)t15) = (t22 & 7U);
    t23 = *((unsigned int *)t13);
    *((unsigned int *)t13) = (t23 & 7U);
    t16 = (t0 + 1208U);
    t17 = *((char **)t16);
    memset(t26, 0, 8);
    xsi_vlog_unsigned_minus(t26, 3, t15, 3, t17, 3);
    t16 = (t0 + 1768);
    t24 = (t0 + 1768);
    t25 = (t24 + 72U);
    t27 = *((char **)t25);
    t40 = (t0 + 1768);
    t41 = (t40 + 64U);
    t47 = *((char **)t41);
    t48 = ((char*)((ng10)));
    xsi_vlog_generic_convert_array_indices(t50, t58, t27, t47, 2, 1, t48, 32, 1);
    t49 = (t0 + 1768);
    t51 = (t49 + 72U);
    t52 = *((char **)t51);
    t53 = ((char*)((ng4)));
    t54 = ((char*)((ng5)));
    xsi_vlog_convert_partindices(t69, t83, t91, ((int*)(t52)), 2, t53, 32, 1, t54, 32, 1);
    t55 = (t50 + 4);
    t28 = *((unsigned int *)t55);
    t113 = (!(t28));
    t56 = (t58 + 4);
    t29 = *((unsigned int *)t56);
    t114 = (!(t29));
    t115 = (t113 && t114);
    t57 = (t69 + 4);
    t30 = *((unsigned int *)t57);
    t116 = (!(t30));
    t117 = (t115 && t116);
    t59 = (t83 + 4);
    t31 = *((unsigned int *)t59);
    t118 = (!(t31));
    t119 = (t117 && t118);
    t60 = (t91 + 4);
    t32 = *((unsigned int *)t60);
    t120 = (!(t32));
    t121 = (t119 && t120);
    if (t121 == 1)
        goto LAB62;

LAB63:    xsi_set_current_line(75, ng0);
    xsi_vlogfile_write(1, 0, 0, ng6, 1, t0);
    goto LAB60;

LAB62:    t33 = *((unsigned int *)t91);
    t122 = (t33 + 0);
    t34 = *((unsigned int *)t58);
    t35 = *((unsigned int *)t83);
    t123 = (t34 + t35);
    t36 = *((unsigned int *)t69);
    t37 = *((unsigned int *)t83);
    t124 = (t36 - t37);
    t125 = (t124 + 1);
    xsi_vlogvar_assign_value(t16, t26, t122, t123, t125);
    goto LAB63;

LAB68:    t25 = (t26 + 4);
    *((unsigned int *)t26) = 1;
    *((unsigned int *)t25) = 1;
    goto LAB69;

LAB70:    xsi_set_current_line(92, ng0);

LAB73:    xsi_set_current_line(93, ng0);
    t40 = (t0 + 1768);
    t41 = (t40 + 56U);
    t47 = *((char **)t41);
    t48 = (t0 + 1768);
    t49 = (t48 + 72U);
    t51 = *((char **)t49);
    t52 = (t0 + 1768);
    t53 = (t52 + 64U);
    t54 = *((char **)t53);
    t55 = ((char*)((ng11)));
    xsi_vlog_generic_get_array_select_value(t50, 8, t47, t51, t54, 2, 1, t55, 32, 1);
    memset(t58, 0, 8);
    t56 = (t58 + 4);
    t57 = (t50 + 4);
    t61 = *((unsigned int *)t50);
    t62 = (t61 >> 3);
    *((unsigned int *)t58) = t62;
    t63 = *((unsigned int *)t57);
    t64 = (t63 >> 3);
    *((unsigned int *)t56) = t64;
    t65 = *((unsigned int *)t58);
    *((unsigned int *)t58) = (t65 & 7U);
    t66 = *((unsigned int *)t56);
    *((unsigned int *)t56) = (t66 & 7U);
    t59 = (t0 + 1208U);
    t60 = *((char **)t59);
    memset(t69, 0, 8);
    t59 = (t58 + 4);
    if (*((unsigned int *)t59) != 0)
        goto LAB75;

LAB74:    t67 = (t60 + 4);
    if (*((unsigned int *)t67) != 0)
        goto LAB75;

LAB78:    if (*((unsigned int *)t58) < *((unsigned int *)t60))
        goto LAB77;

LAB76:    *((unsigned int *)t69) = 1;

LAB77:    t70 = (t69 + 4);
    t73 = *((unsigned int *)t70);
    t74 = (~(t73));
    t75 = *((unsigned int *)t69);
    t76 = (t75 & t74);
    t77 = (t76 != 0);
    if (t77 > 0)
        goto LAB79;

LAB80:    xsi_set_current_line(110, ng0);

LAB95:    xsi_set_current_line(111, ng0);
    t2 = ((char*)((ng8)));
    t3 = (t0 + 1928);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    xsi_set_current_line(112, ng0);
    t2 = (t0 + 1368U);
    t3 = *((char **)t2);
    t2 = (t0 + 2088);
    xsi_vlogvar_assign_value(t2, t3, 0, 0, 4);
    xsi_set_current_line(113, ng0);
    t2 = (t0 + 1208U);
    t3 = *((char **)t2);
    t2 = (t0 + 1768);
    t4 = (t2 + 56U);
    t5 = *((char **)t4);
    t6 = (t0 + 1768);
    t8 = (t6 + 72U);
    t9 = *((char **)t8);
    t10 = (t0 + 1768);
    t11 = (t10 + 64U);
    t12 = *((char **)t11);
    t13 = ((char*)((ng11)));
    xsi_vlog_generic_get_array_select_value(t7, 8, t5, t9, t12, 2, 1, t13, 32, 1);
    memset(t15, 0, 8);
    t14 = (t15 + 4);
    t16 = (t7 + 4);
    t18 = *((unsigned int *)t7);
    t19 = (t18 >> 3);
    *((unsigned int *)t15) = t19;
    t20 = *((unsigned int *)t16);
    t21 = (t20 >> 3);
    *((unsigned int *)t14) = t21;
    t22 = *((unsigned int *)t15);
    *((unsigned int *)t15) = (t22 & 7U);
    t23 = *((unsigned int *)t14);
    *((unsigned int *)t14) = (t23 & 7U);
    memset(t26, 0, 8);
    xsi_vlog_unsigned_multiply(t26, 3, t3, 3, t15, 3);
    xsi_vlogfile_write(1, 0, 0, ng9, 2, t0, (char)118, t26, 3);

LAB81:    goto LAB72;

LAB75:    t68 = (t69 + 4);
    *((unsigned int *)t69) = 1;
    *((unsigned int *)t68) = 1;
    goto LAB77;

LAB79:    xsi_set_current_line(94, ng0);

LAB82:    xsi_set_current_line(95, ng0);
    t71 = (t0 + 1368U);
    t72 = *((char **)t71);
    t71 = (t0 + 1208U);
    t78 = *((char **)t71);
    t71 = (t0 + 1768);
    t79 = (t71 + 56U);
    t80 = *((char **)t79);
    t81 = (t0 + 1768);
    t82 = (t81 + 72U);
    t84 = *((char **)t82);
    t85 = (t0 + 1768);
    t86 = (t85 + 64U);
    t87 = *((char **)t86);
    t88 = ((char*)((ng11)));
    xsi_vlog_generic_get_array_select_value(t83, 8, t80, t84, t87, 2, 1, t88, 32, 1);
    memset(t91, 0, 8);
    t89 = (t91 + 4);
    t90 = (t83 + 4);
    t94 = *((unsigned int *)t83);
    t95 = (t94 >> 0);
    *((unsigned int *)t91) = t95;
    t96 = *((unsigned int *)t90);
    t97 = (t96 >> 0);
    *((unsigned int *)t89) = t97;
    t98 = *((unsigned int *)t91);
    *((unsigned int *)t91) = (t98 & 7U);
    t99 = *((unsigned int *)t89);
    *((unsigned int *)t89) = (t99 & 7U);
    memset(t100, 0, 8);
    xsi_vlog_unsigned_multiply(t100, 4, t78, 3, t91, 4);
    memset(t101, 0, 8);
    t92 = (t72 + 4);
    if (*((unsigned int *)t92) != 0)
        goto LAB84;

LAB83:    t93 = (t100 + 4);
    if (*((unsigned int *)t93) != 0)
        goto LAB84;

LAB87:    if (*((unsigned int *)t72) < *((unsigned int *)t100))
        goto LAB86;

LAB85:    *((unsigned int *)t101) = 1;

LAB86:    t103 = (t101 + 4);
    t106 = *((unsigned int *)t103);
    t107 = (~(t106));
    t108 = *((unsigned int *)t101);
    t109 = (t108 & t107);
    t110 = (t109 != 0);
    if (t110 > 0)
        goto LAB88;

LAB89:    xsi_set_current_line(103, ng0);

LAB94:    xsi_set_current_line(104, ng0);
    t2 = ((char*)((ng8)));
    t3 = (t0 + 1928);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    xsi_set_current_line(105, ng0);
    t2 = (t0 + 1368U);
    t3 = *((char **)t2);
    t2 = (t0 + 2088);
    xsi_vlogvar_assign_value(t2, t3, 0, 0, 4);
    xsi_set_current_line(106, ng0);
    t2 = (t0 + 1208U);
    t3 = *((char **)t2);
    t2 = (t0 + 1768);
    t4 = (t2 + 56U);
    t5 = *((char **)t4);
    t6 = (t0 + 1768);
    t8 = (t6 + 72U);
    t9 = *((char **)t8);
    t10 = (t0 + 1768);
    t11 = (t10 + 64U);
    t12 = *((char **)t11);
    t13 = ((char*)((ng11)));
    xsi_vlog_generic_get_array_select_value(t7, 8, t5, t9, t12, 2, 1, t13, 32, 1);
    memset(t15, 0, 8);
    t14 = (t15 + 4);
    t16 = (t7 + 4);
    t18 = *((unsigned int *)t7);
    t19 = (t18 >> 0);
    *((unsigned int *)t15) = t19;
    t20 = *((unsigned int *)t16);
    t21 = (t20 >> 0);
    *((unsigned int *)t14) = t21;
    t22 = *((unsigned int *)t15);
    *((unsigned int *)t15) = (t22 & 7U);
    t23 = *((unsigned int *)t14);
    *((unsigned int *)t14) = (t23 & 7U);
    memset(t26, 0, 8);
    xsi_vlog_unsigned_multiply(t26, 3, t3, 3, t15, 3);
    xsi_vlogfile_write(1, 0, 0, ng7, 2, t0, (char)118, t26, 3);

LAB90:    goto LAB81;

LAB84:    t102 = (t101 + 4);
    *((unsigned int *)t101) = 1;
    *((unsigned int *)t102) = 1;
    goto LAB86;

LAB88:    xsi_set_current_line(96, ng0);

LAB91:    xsi_set_current_line(97, ng0);
    t104 = ((char*)((ng3)));
    t105 = (t0 + 1928);
    xsi_vlogvar_assign_value(t105, t104, 0, 0, 1);
    xsi_set_current_line(98, ng0);
    t2 = (t0 + 1368U);
    t3 = *((char **)t2);
    t2 = (t0 + 1208U);
    t4 = *((char **)t2);
    t2 = (t0 + 1768);
    t5 = (t2 + 56U);
    t6 = *((char **)t5);
    t8 = (t0 + 1768);
    t9 = (t8 + 72U);
    t10 = *((char **)t9);
    t11 = (t0 + 1768);
    t12 = (t11 + 64U);
    t13 = *((char **)t12);
    t14 = ((char*)((ng11)));
    xsi_vlog_generic_get_array_select_value(t7, 8, t6, t10, t13, 2, 1, t14, 32, 1);
    memset(t15, 0, 8);
    t16 = (t15 + 4);
    t17 = (t7 + 4);
    t18 = *((unsigned int *)t7);
    t19 = (t18 >> 0);
    *((unsigned int *)t15) = t19;
    t20 = *((unsigned int *)t17);
    t21 = (t20 >> 0);
    *((unsigned int *)t16) = t21;
    t22 = *((unsigned int *)t15);
    *((unsigned int *)t15) = (t22 & 7U);
    t23 = *((unsigned int *)t16);
    *((unsigned int *)t16) = (t23 & 7U);
    memset(t26, 0, 8);
    xsi_vlog_unsigned_multiply(t26, 4, t4, 3, t15, 4);
    memset(t50, 0, 8);
    xsi_vlog_unsigned_minus(t50, 4, t3, 4, t26, 4);
    t24 = (t0 + 2088);
    xsi_vlogvar_assign_value(t24, t50, 0, 0, 4);
    xsi_set_current_line(99, ng0);
    t2 = (t0 + 1768);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t0 + 1768);
    t6 = (t5 + 72U);
    t8 = *((char **)t6);
    t9 = (t0 + 1768);
    t10 = (t9 + 64U);
    t11 = *((char **)t10);
    t12 = ((char*)((ng11)));
    xsi_vlog_generic_get_array_select_value(t7, 8, t4, t8, t11, 2, 1, t12, 32, 1);
    memset(t15, 0, 8);
    t13 = (t15 + 4);
    t14 = (t7 + 4);
    t18 = *((unsigned int *)t7);
    t19 = (t18 >> 3);
    *((unsigned int *)t15) = t19;
    t20 = *((unsigned int *)t14);
    t21 = (t20 >> 3);
    *((unsigned int *)t13) = t21;
    t22 = *((unsigned int *)t15);
    *((unsigned int *)t15) = (t22 & 7U);
    t23 = *((unsigned int *)t13);
    *((unsigned int *)t13) = (t23 & 7U);
    t16 = (t0 + 1208U);
    t17 = *((char **)t16);
    memset(t26, 0, 8);
    xsi_vlog_unsigned_minus(t26, 3, t15, 3, t17, 3);
    t16 = (t0 + 1768);
    t24 = (t0 + 1768);
    t25 = (t24 + 72U);
    t27 = *((char **)t25);
    t40 = (t0 + 1768);
    t41 = (t40 + 64U);
    t47 = *((char **)t41);
    t48 = ((char*)((ng11)));
    xsi_vlog_generic_convert_array_indices(t50, t58, t27, t47, 2, 1, t48, 32, 1);
    t49 = (t0 + 1768);
    t51 = (t49 + 72U);
    t52 = *((char **)t51);
    t53 = ((char*)((ng4)));
    t54 = ((char*)((ng5)));
    xsi_vlog_convert_partindices(t69, t83, t91, ((int*)(t52)), 2, t53, 32, 1, t54, 32, 1);
    t55 = (t50 + 4);
    t28 = *((unsigned int *)t55);
    t113 = (!(t28));
    t56 = (t58 + 4);
    t29 = *((unsigned int *)t56);
    t114 = (!(t29));
    t115 = (t113 && t114);
    t57 = (t69 + 4);
    t30 = *((unsigned int *)t57);
    t116 = (!(t30));
    t117 = (t115 && t116);
    t59 = (t83 + 4);
    t31 = *((unsigned int *)t59);
    t118 = (!(t31));
    t119 = (t117 && t118);
    t60 = (t91 + 4);
    t32 = *((unsigned int *)t60);
    t120 = (!(t32));
    t121 = (t119 && t120);
    if (t121 == 1)
        goto LAB92;

LAB93:    xsi_set_current_line(100, ng0);
    xsi_vlogfile_write(1, 0, 0, ng6, 1, t0);
    goto LAB90;

LAB92:    t33 = *((unsigned int *)t91);
    t122 = (t33 + 0);
    t34 = *((unsigned int *)t58);
    t35 = *((unsigned int *)t83);
    t123 = (t34 + t35);
    t36 = *((unsigned int *)t69);
    t37 = *((unsigned int *)t83);
    t124 = (t36 - t37);
    t125 = (t124 + 1);
    xsi_vlogvar_assign_value(t16, t26, t122, t123, t125);
    goto LAB93;

LAB98:    t25 = (t26 + 4);
    *((unsigned int *)t26) = 1;
    *((unsigned int *)t25) = 1;
    goto LAB99;

LAB100:    xsi_set_current_line(117, ng0);

LAB103:    xsi_set_current_line(118, ng0);
    t40 = (t0 + 1768);
    t41 = (t40 + 56U);
    t47 = *((char **)t41);
    t48 = (t0 + 1768);
    t49 = (t48 + 72U);
    t51 = *((char **)t49);
    t52 = (t0 + 1768);
    t53 = (t52 + 64U);
    t54 = *((char **)t53);
    t55 = ((char*)((ng5)));
    xsi_vlog_generic_get_array_select_value(t50, 8, t47, t51, t54, 2, 1, t55, 32, 1);
    memset(t58, 0, 8);
    t56 = (t58 + 4);
    t57 = (t50 + 4);
    t61 = *((unsigned int *)t50);
    t62 = (t61 >> 3);
    *((unsigned int *)t58) = t62;
    t63 = *((unsigned int *)t57);
    t64 = (t63 >> 3);
    *((unsigned int *)t56) = t64;
    t65 = *((unsigned int *)t58);
    *((unsigned int *)t58) = (t65 & 7U);
    t66 = *((unsigned int *)t56);
    *((unsigned int *)t56) = (t66 & 7U);
    t59 = (t0 + 1208U);
    t60 = *((char **)t59);
    memset(t69, 0, 8);
    t59 = (t58 + 4);
    if (*((unsigned int *)t59) != 0)
        goto LAB105;

LAB104:    t67 = (t60 + 4);
    if (*((unsigned int *)t67) != 0)
        goto LAB105;

LAB108:    if (*((unsigned int *)t58) < *((unsigned int *)t60))
        goto LAB107;

LAB106:    *((unsigned int *)t69) = 1;

LAB107:    t70 = (t69 + 4);
    t73 = *((unsigned int *)t70);
    t74 = (~(t73));
    t75 = *((unsigned int *)t69);
    t76 = (t75 & t74);
    t77 = (t76 != 0);
    if (t77 > 0)
        goto LAB109;

LAB110:    xsi_set_current_line(135, ng0);

LAB125:    xsi_set_current_line(136, ng0);
    t2 = ((char*)((ng8)));
    t3 = (t0 + 1928);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    xsi_set_current_line(137, ng0);
    t2 = (t0 + 1368U);
    t3 = *((char **)t2);
    t2 = (t0 + 2088);
    xsi_vlogvar_assign_value(t2, t3, 0, 0, 4);
    xsi_set_current_line(138, ng0);
    t2 = (t0 + 1208U);
    t3 = *((char **)t2);
    t2 = (t0 + 1768);
    t4 = (t2 + 56U);
    t5 = *((char **)t4);
    t6 = (t0 + 1768);
    t8 = (t6 + 72U);
    t9 = *((char **)t8);
    t10 = (t0 + 1768);
    t11 = (t10 + 64U);
    t12 = *((char **)t11);
    t13 = ((char*)((ng5)));
    xsi_vlog_generic_get_array_select_value(t7, 8, t5, t9, t12, 2, 1, t13, 32, 1);
    memset(t15, 0, 8);
    t14 = (t15 + 4);
    t16 = (t7 + 4);
    t18 = *((unsigned int *)t7);
    t19 = (t18 >> 3);
    *((unsigned int *)t15) = t19;
    t20 = *((unsigned int *)t16);
    t21 = (t20 >> 3);
    *((unsigned int *)t14) = t21;
    t22 = *((unsigned int *)t15);
    *((unsigned int *)t15) = (t22 & 7U);
    t23 = *((unsigned int *)t14);
    *((unsigned int *)t14) = (t23 & 7U);
    memset(t26, 0, 8);
    xsi_vlog_unsigned_multiply(t26, 3, t3, 3, t15, 3);
    xsi_vlogfile_write(1, 0, 0, ng9, 2, t0, (char)118, t26, 3);

LAB111:    goto LAB102;

LAB105:    t68 = (t69 + 4);
    *((unsigned int *)t69) = 1;
    *((unsigned int *)t68) = 1;
    goto LAB107;

LAB109:    xsi_set_current_line(119, ng0);

LAB112:    xsi_set_current_line(120, ng0);
    t71 = (t0 + 1368U);
    t72 = *((char **)t71);
    t71 = (t0 + 1208U);
    t78 = *((char **)t71);
    t71 = (t0 + 1768);
    t79 = (t71 + 56U);
    t80 = *((char **)t79);
    t81 = (t0 + 1768);
    t82 = (t81 + 72U);
    t84 = *((char **)t82);
    t85 = (t0 + 1768);
    t86 = (t85 + 64U);
    t87 = *((char **)t86);
    t88 = ((char*)((ng5)));
    xsi_vlog_generic_get_array_select_value(t83, 8, t80, t84, t87, 2, 1, t88, 32, 1);
    memset(t91, 0, 8);
    t89 = (t91 + 4);
    t90 = (t83 + 4);
    t94 = *((unsigned int *)t83);
    t95 = (t94 >> 0);
    *((unsigned int *)t91) = t95;
    t96 = *((unsigned int *)t90);
    t97 = (t96 >> 0);
    *((unsigned int *)t89) = t97;
    t98 = *((unsigned int *)t91);
    *((unsigned int *)t91) = (t98 & 7U);
    t99 = *((unsigned int *)t89);
    *((unsigned int *)t89) = (t99 & 7U);
    memset(t100, 0, 8);
    xsi_vlog_unsigned_multiply(t100, 4, t78, 3, t91, 4);
    memset(t101, 0, 8);
    t92 = (t72 + 4);
    if (*((unsigned int *)t92) != 0)
        goto LAB114;

LAB113:    t93 = (t100 + 4);
    if (*((unsigned int *)t93) != 0)
        goto LAB114;

LAB117:    if (*((unsigned int *)t72) < *((unsigned int *)t100))
        goto LAB116;

LAB115:    *((unsigned int *)t101) = 1;

LAB116:    t103 = (t101 + 4);
    t106 = *((unsigned int *)t103);
    t107 = (~(t106));
    t108 = *((unsigned int *)t101);
    t109 = (t108 & t107);
    t110 = (t109 != 0);
    if (t110 > 0)
        goto LAB118;

LAB119:    xsi_set_current_line(128, ng0);

LAB124:    xsi_set_current_line(129, ng0);
    t2 = ((char*)((ng8)));
    t3 = (t0 + 1928);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    xsi_set_current_line(130, ng0);
    t2 = (t0 + 1368U);
    t3 = *((char **)t2);
    t2 = (t0 + 2088);
    xsi_vlogvar_assign_value(t2, t3, 0, 0, 4);
    xsi_set_current_line(131, ng0);
    t2 = (t0 + 1208U);
    t3 = *((char **)t2);
    t2 = (t0 + 1768);
    t4 = (t2 + 56U);
    t5 = *((char **)t4);
    t6 = (t0 + 1768);
    t8 = (t6 + 72U);
    t9 = *((char **)t8);
    t10 = (t0 + 1768);
    t11 = (t10 + 64U);
    t12 = *((char **)t11);
    t13 = ((char*)((ng5)));
    xsi_vlog_generic_get_array_select_value(t7, 8, t5, t9, t12, 2, 1, t13, 32, 1);
    memset(t15, 0, 8);
    t14 = (t15 + 4);
    t16 = (t7 + 4);
    t18 = *((unsigned int *)t7);
    t19 = (t18 >> 0);
    *((unsigned int *)t15) = t19;
    t20 = *((unsigned int *)t16);
    t21 = (t20 >> 0);
    *((unsigned int *)t14) = t21;
    t22 = *((unsigned int *)t15);
    *((unsigned int *)t15) = (t22 & 7U);
    t23 = *((unsigned int *)t14);
    *((unsigned int *)t14) = (t23 & 7U);
    memset(t26, 0, 8);
    xsi_vlog_unsigned_multiply(t26, 3, t3, 3, t15, 3);
    xsi_vlogfile_write(1, 0, 0, ng7, 2, t0, (char)118, t26, 3);

LAB120:    goto LAB111;

LAB114:    t102 = (t101 + 4);
    *((unsigned int *)t101) = 1;
    *((unsigned int *)t102) = 1;
    goto LAB116;

LAB118:    xsi_set_current_line(121, ng0);

LAB121:    xsi_set_current_line(122, ng0);
    t104 = ((char*)((ng3)));
    t105 = (t0 + 1928);
    xsi_vlogvar_assign_value(t105, t104, 0, 0, 1);
    xsi_set_current_line(123, ng0);
    t2 = (t0 + 1368U);
    t3 = *((char **)t2);
    t2 = (t0 + 1208U);
    t4 = *((char **)t2);
    t2 = (t0 + 1768);
    t5 = (t2 + 56U);
    t6 = *((char **)t5);
    t8 = (t0 + 1768);
    t9 = (t8 + 72U);
    t10 = *((char **)t9);
    t11 = (t0 + 1768);
    t12 = (t11 + 64U);
    t13 = *((char **)t12);
    t14 = ((char*)((ng5)));
    xsi_vlog_generic_get_array_select_value(t7, 8, t6, t10, t13, 2, 1, t14, 32, 1);
    memset(t15, 0, 8);
    t16 = (t15 + 4);
    t17 = (t7 + 4);
    t18 = *((unsigned int *)t7);
    t19 = (t18 >> 0);
    *((unsigned int *)t15) = t19;
    t20 = *((unsigned int *)t17);
    t21 = (t20 >> 0);
    *((unsigned int *)t16) = t21;
    t22 = *((unsigned int *)t15);
    *((unsigned int *)t15) = (t22 & 7U);
    t23 = *((unsigned int *)t16);
    *((unsigned int *)t16) = (t23 & 7U);
    memset(t26, 0, 8);
    xsi_vlog_unsigned_multiply(t26, 4, t4, 3, t15, 4);
    memset(t50, 0, 8);
    xsi_vlog_unsigned_minus(t50, 4, t3, 4, t26, 4);
    t24 = (t0 + 2088);
    xsi_vlogvar_assign_value(t24, t50, 0, 0, 4);
    xsi_set_current_line(124, ng0);
    t2 = (t0 + 1768);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t0 + 1768);
    t6 = (t5 + 72U);
    t8 = *((char **)t6);
    t9 = (t0 + 1768);
    t10 = (t9 + 64U);
    t11 = *((char **)t10);
    t12 = ((char*)((ng5)));
    xsi_vlog_generic_get_array_select_value(t7, 8, t4, t8, t11, 2, 1, t12, 32, 1);
    memset(t15, 0, 8);
    t13 = (t15 + 4);
    t14 = (t7 + 4);
    t18 = *((unsigned int *)t7);
    t19 = (t18 >> 3);
    *((unsigned int *)t15) = t19;
    t20 = *((unsigned int *)t14);
    t21 = (t20 >> 3);
    *((unsigned int *)t13) = t21;
    t22 = *((unsigned int *)t15);
    *((unsigned int *)t15) = (t22 & 7U);
    t23 = *((unsigned int *)t13);
    *((unsigned int *)t13) = (t23 & 7U);
    t16 = (t0 + 1208U);
    t17 = *((char **)t16);
    memset(t26, 0, 8);
    xsi_vlog_unsigned_minus(t26, 3, t15, 3, t17, 3);
    t16 = (t0 + 1768);
    t24 = (t0 + 1768);
    t25 = (t24 + 72U);
    t27 = *((char **)t25);
    t40 = (t0 + 1768);
    t41 = (t40 + 64U);
    t47 = *((char **)t41);
    t48 = ((char*)((ng5)));
    xsi_vlog_generic_convert_array_indices(t50, t58, t27, t47, 2, 1, t48, 32, 1);
    t49 = (t0 + 1768);
    t51 = (t49 + 72U);
    t52 = *((char **)t51);
    t53 = ((char*)((ng4)));
    t54 = ((char*)((ng5)));
    xsi_vlog_convert_partindices(t69, t83, t91, ((int*)(t52)), 2, t53, 32, 1, t54, 32, 1);
    t55 = (t50 + 4);
    t28 = *((unsigned int *)t55);
    t113 = (!(t28));
    t56 = (t58 + 4);
    t29 = *((unsigned int *)t56);
    t114 = (!(t29));
    t115 = (t113 && t114);
    t57 = (t69 + 4);
    t30 = *((unsigned int *)t57);
    t116 = (!(t30));
    t117 = (t115 && t116);
    t59 = (t83 + 4);
    t31 = *((unsigned int *)t59);
    t118 = (!(t31));
    t119 = (t117 && t118);
    t60 = (t91 + 4);
    t32 = *((unsigned int *)t60);
    t120 = (!(t32));
    t121 = (t119 && t120);
    if (t121 == 1)
        goto LAB122;

LAB123:    xsi_set_current_line(125, ng0);
    xsi_vlogfile_write(1, 0, 0, ng6, 1, t0);
    goto LAB120;

LAB122:    t33 = *((unsigned int *)t91);
    t122 = (t33 + 0);
    t34 = *((unsigned int *)t58);
    t35 = *((unsigned int *)t83);
    t123 = (t34 + t35);
    t36 = *((unsigned int *)t69);
    t37 = *((unsigned int *)t83);
    t124 = (t36 - t37);
    t125 = (t124 + 1);
    xsi_vlogvar_assign_value(t16, t26, t122, t123, t125);
    goto LAB123;

}

static void Initial_149_2(char *t0)
{
    char t1[8];
    char *t2;
    char *t3;

LAB0:    xsi_set_current_line(150, ng0);

LAB2:    xsi_set_current_line(151, ng0);
    *((int *)t1) = xsi_vlogfile_file_open_of_cname_ctype(ng1, ng13);
    t2 = (t1 + 4);
    *((int *)t2) = 0;
    t3 = (t0 + 2248);
    xsi_vlogvar_assign_value(t3, t1, 0, 0, 32);

LAB1:    return;
}

static void Initial_154_3(char *t0)
{
    char t7[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;

LAB0:    xsi_set_current_line(155, ng0);

LAB2:    xsi_set_current_line(156, ng0);
    t1 = (t0 + 2248);
    t2 = (t1 + 56U);
    t3 = *((char **)t2);
    t4 = (t0 + 1768);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t8 = (t0 + 1768);
    t9 = (t8 + 72U);
    t10 = *((char **)t9);
    t11 = (t0 + 1768);
    t12 = (t11 + 64U);
    t13 = *((char **)t12);
    t14 = ((char*)((ng2)));
    xsi_vlog_generic_get_array_select_value(t7, 8, t6, t10, t13, 2, 1, t14, 32, 1);
    xsi_vlogfile_fwrite(*((unsigned int *)t3), 0, 0, 1, ng14, 2, t0, (char)118, t7, 8);
    xsi_set_current_line(157, ng0);
    t1 = (t0 + 2248);
    t2 = (t1 + 56U);
    t3 = *((char **)t2);
    t4 = (t0 + 1768);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t8 = (t0 + 1768);
    t9 = (t8 + 72U);
    t10 = *((char **)t9);
    t11 = (t0 + 1768);
    t12 = (t11 + 64U);
    t13 = *((char **)t12);
    t14 = ((char*)((ng10)));
    xsi_vlog_generic_get_array_select_value(t7, 8, t6, t10, t13, 2, 1, t14, 32, 1);
    xsi_vlogfile_fwrite(*((unsigned int *)t3), 0, 0, 1, ng14, 2, t0, (char)118, t7, 8);
    xsi_set_current_line(158, ng0);
    t1 = (t0 + 2248);
    t2 = (t1 + 56U);
    t3 = *((char **)t2);
    t4 = (t0 + 1768);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t8 = (t0 + 1768);
    t9 = (t8 + 72U);
    t10 = *((char **)t9);
    t11 = (t0 + 1768);
    t12 = (t11 + 64U);
    t13 = *((char **)t12);
    t14 = ((char*)((ng11)));
    xsi_vlog_generic_get_array_select_value(t7, 8, t6, t10, t13, 2, 1, t14, 32, 1);
    xsi_vlogfile_fwrite(*((unsigned int *)t3), 0, 0, 1, ng14, 2, t0, (char)118, t7, 8);
    xsi_set_current_line(159, ng0);
    t1 = (t0 + 2248);
    t2 = (t1 + 56U);
    t3 = *((char **)t2);
    t4 = (t0 + 1768);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t8 = (t0 + 1768);
    t9 = (t8 + 72U);
    t10 = *((char **)t9);
    t11 = (t0 + 1768);
    t12 = (t11 + 64U);
    t13 = *((char **)t12);
    t14 = ((char*)((ng5)));
    xsi_vlog_generic_get_array_select_value(t7, 8, t6, t10, t13, 2, 1, t14, 32, 1);
    xsi_vlogfile_fwrite(*((unsigned int *)t3), 0, 0, 1, ng14, 2, t0, (char)118, t7, 8);

LAB1:    return;
}

static void Initial_161_4(char *t0)
{
    char *t1;
    char *t2;
    char *t3;

LAB0:    xsi_set_current_line(162, ng0);

LAB2:    xsi_set_current_line(163, ng0);
    t1 = (t0 + 2248);
    t2 = (t1 + 56U);
    t3 = *((char **)t2);
    xsi_vlogfile_fclose(*((unsigned int *)t3));

LAB1:    return;
}


extern void work_m_00000000003499240806_0038339330_init()
{
	static char *pe[] = {(void *)Initial_25_0,(void *)Always_39_1,(void *)Initial_149_2,(void *)Initial_154_3,(void *)Initial_161_4};
	xsi_register_didat("work_m_00000000003499240806_0038339330", "isim/VendingMachineTest_isim_beh.exe.sim/work/m_00000000003499240806_0038339330.didat");
	xsi_register_executes(pe);
}
