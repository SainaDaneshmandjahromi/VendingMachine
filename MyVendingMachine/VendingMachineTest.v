`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   11:45:55 07/04/2019
// Design Name:   VendingMachine
// Module Name:   D:/Saina Files/logic circuit/MyVendingMachine/VendingMachineTest.v
// Project Name:  MyVendingMachine
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: VendingMachine
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module VendingMachineTest;

	// Inputs
	reg [1:0] Tag;
	reg [2:0] Count;
	reg [3:0] Money;

	// Outputs
	wire Possiblity;
	wire [3:0] RemainingMoney;

	// Instantiate the Unit Under Test (UUT)
	VendingMachine uut (
		.Tag(Tag), 
		.Count(Count), 
		.Money(Money), 
		.Possiblity(Possiblity), 
		.RemainingMoney(RemainingMoney)
	);

	initial begin
		// Initialize Inputs
		Tag = 0;
		Count = 0;
		Money = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
        // Add stimulus here
        
        Tag = 2'b11;
        Count = 3'b010;
        Money = 4'b1000;    
        


	end
      
endmodule

