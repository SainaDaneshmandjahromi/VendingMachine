`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:51:21 06/29/2019 
// Design Name: 
// Module Name:    VendingMachine 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module VendingMachine(Tag, Count, Money, Possiblity, RemainingMoney);
 
reg [7:0] mem [3:0];

initial
	begin 
	$readmemb("MyVending.txt", mem);
	end
	
	input [1:0] Tag;
	input [2:0] Count;
	input [3:0] Money;
	
	output reg Possiblity;
	output reg [3:0] RemainingMoney;
	
	integer f;
		
	always @(Tag or Count or Money) 
		begin
			if(mem[0][7:6] == Tag)
				begin
					if(mem[0][5:3] >= Count)
						begin
							if(Money >= Count * mem[0][2:0])
								begin
									Possiblity = 1'b1;
									RemainingMoney = Money - Count * mem[0][2:0];
									mem[0][5:3] = mem[0][5:3] - Count;
									$display("The item is bought successfully");
								end
							else
								begin
									Possiblity = 0;
									RemainingMoney = Money ;
									$display("You don't have enough money you need to have %b money", Count * mem[0][2:0]);
								end
						end
					else
						begin
							Possiblity = 1'b0;
							RemainingMoney = Money ;
							$display("We just have %b of this item", Count * mem[0][5:3]);
						end
				end
			else if(mem[1][7:6] == Tag)
				begin
					if(mem[1][5:3] >= Count)
						begin
							if(Money >= Count * mem[1][2:0])
								begin
									Possiblity = 1'b1;
									RemainingMoney = Money - Count * mem[1][2:0];
									mem[1][5:3] = mem[1][5:3] - Count;
									$display("The item is bought successfully");
								end
							else
								begin
									Possiblity = 1'b0;
									RemainingMoney = Money ;
									$display("You don't have enough money you need to have %b money", Count * mem[1][2:0]);
								end
						end
					else
						begin
							Possiblity = 1'b0;
							RemainingMoney = Money ;
							$display("We just have %b of this item", Count * mem[1][5:3]);
						end
				end
			else if(mem[2][7:6] == Tag)
				begin
					if(mem[2][5:3] >= Count)
						begin
							if(Money >= Count * mem[2][2:0])
								begin
									Possiblity = 1'b1;
									RemainingMoney = Money - Count * mem[2][2:0];
									mem[2][5:3] = mem[2][5:3] - Count;
									$display("The item is bought successfully");
								end
							else
								begin
									Possiblity = 1'b0;
									RemainingMoney = Money ;
									$display("You don't have enough money you need to have %b money", Count * mem[2][2:0]);
								end
						end
					else
						begin
							Possiblity = 1'b0;
							RemainingMoney = Money ;
							$display("We just have %b of this item", Count * mem[2][5:3]);
						end
				end
			else if(mem[3][7:6] == Tag)
				begin
					if(mem[3][5:3] >= Count)
						begin
							if(Money >= Count * mem[3][2:0])
								begin
									Possiblity = 1'b1;
									RemainingMoney = Money - Count * mem[3][2:0];
									mem[3][5:3] = mem[3][5:3] - Count;
									$display("The item is bought successfully");
								end
							else
								begin
									Possiblity = 1'b0;
									RemainingMoney = Money ;
									$display("You don't have enough money you need to have %b money", Count * mem[3][2:0]);
								end
						end
					else
						begin
							Possiblity = 1'b0;
							RemainingMoney = Money ;
							$display("We just have %b of this item", Count * mem[3][5:3]);
						end
				end
			else
				begin
					Possiblity = 1'b0;
					RemainingMoney = Money ;
					$display("We don't have this item in our vending machine");
				end
					
		end
		initial 
			begin
				f = $fopen("MyVending.txt","w");
			end

		initial
			begin
				$fwrite(f,"%b\n",mem[0]);
				$fwrite(f,"%b\n",mem[1]);
				$fwrite(f,"%b\n",mem[2]);
				$fwrite(f,"%b\n",mem[3]);	
			end
		initial 
			begin
				$fclose(f);  
			end
		
		
			
endmodule
